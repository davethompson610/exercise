/*
Service API Unit Tests
*/

package api

import (
	"bytes"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

// Verify the specified url gets the proper returned status
func verifyUrl(t *testing.T, router *mux.Router, url string, expectedUrlStatus UrlStatus, expectedMalewareType string) {
	// Send a request for the url which is unsafe
	request, _ := http.NewRequest("GET",  "/urlinfo/1/" + url, nil)
	response := httptest.NewRecorder()
	router.ServeHTTP(response, request)
	assert.Equal(t, http.StatusOK, response.Code, "OK response is expected")

	// Check response
	var urlInfo GetUrlInfoResponse
	err := json.NewDecoder(response.Body).Decode(&urlInfo)
	assert.Nil(t, err, "Error decoding response")
	assert.Equal(t, url, urlInfo.URL, "Incorrect url in response")
	assert.Equal(t, expectedUrlStatus.String(), urlInfo.Status, "Incorrect status response")
	assert.Equal(t, expectedMalewareType, urlInfo.MalwareType, "Incorrect maleware type in response")
}

func TestServiceApi(t *testing.T) {
	// Cleanup the DB when done
	defer CleanupDb()

	// Initialize the database to be used by the test
	DbListArg = "malicious;" + DbUriDefault + ";TestDb;Malicious"
	InitDbList()
	CleanupDb()
	InitDbCollections()
	assert.Equal(t, 1, len(DbList), "Wrong number of databases")

	// Insert a url directly into the DB to mark it as unsafe (cache will have to learn this)
	url := UnsafeUrl{"www.malwarebytes.com", "malware", "Trojan"}
	err := InsertDbOneUnsafeUrl(0, url)
	assert.Nil(t, err, "Error inserting into DB")

	// Make a router
	router := MakeRouter()

	// Send a request for the url which is unsafe
	verifyUrl(t, router, url.Authority + "/" + url.PathQuery, UrlStatusNotSafe, url.MalwareType)

	// Send a request for the url which is safe
	verifyUrl(t, router, url.Authority + "/clean", UrlStatusSafe, "")
}

func RunBenchmarkTest(b *testing.B, cacheSize uint64) {
	// Setup for benchmark run
	DbListArg = "default;" + DbUriDefault + ";TestDb;Malicious"
	InitDbList()
	CleanupDb()
	InitDbCollections()
	CacheUsageSizeMax = cacheSize
	UrlLookupCache.Clear()

	// Insert a url
	router := MakeRouter()
	urlInfoReq := &AddUrlInfoRequest{URL:"www.malwarebytes.com/malware", MalwareType:"Trojan"}
	body := new(bytes.Buffer)
	json.NewEncoder(body).Encode(urlInfoReq)
	request, _ := http.NewRequest("POST",  "/urlinfo/1/url", body)
	response := httptest.NewRecorder()
	router.ServeHTTP(response, request)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		request, _ = http.NewRequest("GET",  "/urlinfo/1/" + urlInfoReq.URL, nil)
		response = httptest.NewRecorder()
		router.ServeHTTP(response, request)
		if response.Code != http.StatusOK {
			log.Fatal("Bad response code : ", response.Code)
		}
		var urlInfo GetUrlInfoResponse
		err := json.NewDecoder(response.Body).Decode(&urlInfo)
		if err != nil {
			log.Fatal("Bad response : ", err)
		}
		if urlInfo.URL != urlInfoReq.URL {
			log.Fatal("Bad response URL : ", urlInfo.URL)
		}
		if urlInfo.Status != UrlStatusNotSafe.String() {
			log.Fatal("Bad response status ", urlInfo.Status, " on iter ", i)
		}
		if urlInfo.MalwareType != urlInfoReq.MalwareType {
			log.Fatal("Bad response malwareType : ", urlInfo.MalwareType)
		}
	}
}

func BenchmarkServiceApiWithoutCache(b *testing.B) {
	RunBenchmarkTest(b, 0)
}

func BenchmarkServiceApiWithCache(b *testing.B) {
	RunBenchmarkTest(b, 1024 * 1024 * 1024)
}
