/*
Web Service API.
Utilizes Gorilla Mux for the HTTP service.
*/

package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

// This defines the JSON data received for a urlinfo PUSH request
type AddUrlInfoRequest struct {
	URL         string `json:"url"`
	MalwareType string `json:"malwaretype"`
	Database    string `json:"database"`
}

// This defines the JSON data returned for a urlinfo GET request
type GetUrlInfoResponse struct {
	URL         string `json:"url"`
	Status      string `json:"status"`
	MalwareType string `json:"malwaretype"`
}

// Type of status values returned in response above
type UrlStatus int
const (
	UrlStatusUnknown UrlStatus = iota
	UrlStatusSafe
	UrlStatusNotSafe
)
func (status UrlStatus) String() string {
	return [...]string{"Unknown", "Safe", "NotSafe"}[status]
}

// Handler for a POST /urlinfo/1/addurl request
func addUrlInfoV1(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var urlInfo AddUrlInfoRequest
	err := json.NewDecoder(r.Body).Decode(&urlInfo)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if len(urlInfo.Database) == 0 {
		urlInfo.Database = "default"
	}
	var dbId int
	dbId, err = LookupDb(urlInfo.Database)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var unsafeUrl UnsafeUrl
	unsafeUrl.SetUrl(urlInfo.URL)
	unsafeUrl.MalwareType = urlInfo.MalwareType
	err = InsertDbOneUnsafeUrl(dbId, unsafeUrl)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	UrlLookupCache.AddUrl(urlInfo.URL, UrlStatusNotSafe, urlInfo.MalwareType)
	json.NewEncoder(w).Encode(&urlInfo)
}

// Handler for a GET /urlinfo/1/... request
func getUrlInfoV1(w http.ResponseWriter, r *http.Request) {
	// Get the url to be checked from the request
	var url UnsafeUrl
	url.Authority = mux.Vars(r)["hostname_and_port"]
	url.PathQuery = mux.Vars(r)["original_path_and_query_string"]
	if len(r.URL.RawQuery) > 0 {
		url.PathQuery += "?" + r.URL.RawQuery
	}

	// Check if the URL is safe
	status, malewareType := UrlLookupCache.Lookup(url.String())
	if status == UrlStatusUnknown {
		findUrl := FindUrl{url.Authority, url.PathQuery}
		unsafeUrl, _ := FindDbAllUnsafeUrl(findUrl)
		if unsafeUrl != nil {
			malewareType = unsafeUrl.MalwareType
			status = UrlStatusNotSafe
		} else {
			status = UrlStatusSafe
		}
		UrlLookupCache.AddUrl(url.String(), status, malewareType)
	}

	// Return response
	respInfo := GetUrlInfoResponse{URL: url.String(), Status: status.String(), MalwareType: malewareType}
	json.NewEncoder(w).Encode(respInfo)
}

// Add command line args for the web service
func AddWebServiceCmdArgs() {
	// Nothing yet...
}

// Make a router instance
func MakeRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/urlinfo/1/url", addUrlInfoV1).Methods("POST")
	router.HandleFunc("/urlinfo/1/{hostname_and_port}", getUrlInfoV1).Methods("GET")
	router.HandleFunc("/urlinfo/1/{hostname_and_port}/{original_path_and_query_string:.*}", getUrlInfoV1).Methods("GET")
	return router
}

// Run the web service
func RunWebService() {
	log.Fatal(http.ListenAndServe(":8080", MakeRouter()))
}