/*
Database API.
Utilizes Mongo for the DB service.
*/

package api

import (
	"context"
	"errors"
	"flag"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/integration/mtest"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// DB definitions
const (
	DbUriDefault                     = "mongodb://localhost:27017"
	DbCollectionDocPropAuthName      = "authority"
	DbCollectionDocPropPathQueryName = "pathquery"
	DbCollectionIndexName            = "FindByAuthPathQuery"
	DbErrorDuplicateKey              = 11000
)

// Definition of a Malware database
type DbInfo struct {
	id             int
	name           string
	uri            string
	dbName         string
	collectionName string
}

// The list of databases to be queried
var DbList []DbInfo
var DbMap = make(map[string]*DbInfo)

// Argument defining the databases to be used.
// Format: name;{URI};{DbName};{CollectionName}[,name;{URI};{DbName};{CollectionName}]...
var DbListArg = "default;" + DbUriDefault + ";MalwareDb;UnsafeUrls"

// Definition of an index on a collection
type DbIndex struct {
	Key   map[string]int
	Name  string
}

// Definition of a document in a database
type UnsafeUrl struct {
	Authority   string
	PathQuery   string
	MalwareType string
}
func (url *UnsafeUrl) String() string {
	result := url.Authority
	if len(url.PathQuery) > 0 {
		result += "/" + url.PathQuery
	}
	return result
}
func (url *UnsafeUrl) SetUrl(unsafeUrl string) {
	url.Authority = ""
	url.PathQuery = ""
	tokens := strings.SplitN(unsafeUrl, "/", 2)
	if len(tokens) > 0 {
		url.Authority = tokens[0]
		if len(tokens) > 1 {
			url.PathQuery = tokens[1]
		}
	}
}

// Database query filter
type FindUrl struct {
	Authority   string
	PathQuery   string
}

// Get the URI for a DB
func GetDbUri(dbId int) string {
	if dbId < len(DbList) {
		return DbList[dbId].uri
	}
	return "unknown"
}

// Get a client connection to the DB
func GetDbClient(dbId int) (*mongo.Client, error) {
	if dbId >= len(DbList) {
		return nil, errors.New("invalid database index " + strconv.Itoa(dbId))
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	return mongo.Connect(ctx, options.Client().ApplyURI(GetDbUri(dbId)))
}

// Get the DB collection containing the unsafe URLs
func GetDbCollectionUnsafeUrl(client *mongo.Client, dbId int) *mongo.Collection {
	return client.Database(DbList[dbId].dbName).Collection(DbList[dbId].collectionName)
}

// Check if the specified index exists in the index view
func DbIndexExists(collection *mongo.Collection, index DbIndex) bool {
	cursor, err := collection.Indexes().List(context.Background())
	if err == nil {
		for cursor.Next(mtest.Background) {
			var nextIndex DbIndex
			err = cursor.Decode(&nextIndex)
			if err != nil {
				break
			}
			if reflect.DeepEqual(nextIndex.Key, index.Key) && nextIndex.Name == index.Name {
				return true
			}
		}
	}
	return false
}

// Check if the specified error is a duplicate key type error
func DbErrorDupKey(err error) bool {
	if err != nil {
		we, ok := err.(mongo.WriteException)
		if ok && len(we.WriteErrors) == 1 && we.WriteErrors[0].Code == DbErrorDuplicateKey {
			return true
		}
	}
	return false
}

// Add command line args for the DB service
func AddDbCmdArgs() {
	flag.StringVar(&DbListArg, "databases", DbListArg, "List of databases to be used")
}

// Create the list of databases by Parsing the database list argument
func InitDbList() {
	if len(DbMap) > 0 {
		// Clear the database map and list
		DbMap = make(map[string]*DbInfo)
		DbList = nil
	}
	dbArgs := strings.Split(DbListArg, ",")
	for _, dbArg := range dbArgs {
		dbInfo := strings.Split(dbArg, ";")
		if len(dbInfo) != 4 {
			log.Fatal("Bad database description ", dbArg)
		}
		_, ok := DbMap[dbInfo[0]]
		if ok {
			log.Fatal("Database already defined ", dbArg)
		}
		dbId := len(DbList)
		DbList = append(DbList, DbInfo{dbId, dbInfo[0], dbInfo[1], dbInfo[2], dbInfo[3]})
		DbMap[dbInfo[0]] = &DbList[dbId]
	}
	if len(DbMap) == 0 {
		log.Fatal("No databases defined")
	}
}

// Lookup a database by name and return the ID
func LookupDb(name string) (int, error) {
	dbInfo, ok := DbMap[name]
	if ok {
		return dbInfo.id, nil
	}
	return 0, errors.New("invalid database name " + name)
}

// Verify the databases can be connected to
func CheckDbConnections() {
	// Check connection to each database
	for dbId := range DbList {
		client, err := GetDbClient(dbId)
		if err == nil {
			ctx, _ := context.WithTimeout(context.Background(), 2*time.Second)
			err = client.Ping(ctx, readpref.Primary())
		}
		if err != nil {
			log.Fatal("Unable to connect to ", GetDbUri(dbId), " : ", err)
		}
		client.Disconnect(context.Background())
	}
}

// Initialize the DB collections. This will create the db, collection, and indexes needed.
func InitDbCollections() {
	for dbId := range DbList {
		client, err := GetDbClient(dbId)
		if err != nil {
			log.Fatal("Unable to connect to ", GetDbUri(dbId), " : ", err)
		}
		collection := GetDbCollectionUnsafeUrl(client, dbId)
		indexView := collection.Indexes()
		ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
		_, err = indexView.CreateOne(ctx, mongo.IndexModel{
			Keys: bson.D{
				{DbCollectionDocPropAuthName, 1},
				{DbCollectionDocPropPathQueryName, 1},
			},
			Options: options.Index().SetName(DbCollectionIndexName).SetUnique(true),
		})
		if err != nil {
			log.Fatal("Unable to create index ", DbCollectionIndexName, " : ", err)
		}
		client.Disconnect(context.Background())
	}
}

// Cleanup the database (this deletes the DB)
func CleanupDb() {
	for dbId := range DbList {
		client, err := GetDbClient(dbId)
		if err != nil {
			log.Fatal("Unable to connect to ", GetDbUri(dbId), " : ", err)
		}
		client.Database(DbList[dbId].dbName).Drop(context.Background())
		client.Disconnect(context.Background())
	}
}

// Insert a URL into the Unsafe URL collection
func InsertDbOneUnsafeUrl(dbId int, url UnsafeUrl) error {
	client, err := GetDbClient(dbId)
	if err != nil {
		log.Fatal("Unable to connect to ", GetDbUri(dbId), " : ", err)
	}
	collection := GetDbCollectionUnsafeUrl(client, dbId)
	_, err = collection.InsertOne(context.Background(), url)
	client.Disconnect(context.Background())
	return err
}

// Check if a document for the specified URL is in one of the databases and return it
func FindDbAllUnsafeUrl(findUrl FindUrl) (*UnsafeUrl, error) {
	for dbId := range DbList {
		elem, err := findDbUnsafeUrl(dbId, findUrl)
		if elem != nil {
			return elem, err
		}
	}
	return nil, nil
}

// Check if a document for the specified URL is in a specific database and return it
func findDbUnsafeUrl(dbId int, findUrl FindUrl) (*UnsafeUrl, error) {
	client, err := GetDbClient(dbId)
	if err != nil {
		log.Fatal("Unable to connect to ", GetDbUri(dbId), " : ", err)
	}
	defer client.Disconnect(context.Background())
	collection := GetDbCollectionUnsafeUrl(client, dbId)
	cursor, findErr := collection.Find(context.Background(), findUrl)
	if findErr != nil {
		return nil, findErr
	}
	if cursor.Next(context.Background()) {
		var elem UnsafeUrl
		err = cursor.Decode(&elem)
		if err != nil {
			return nil, err
		}
		return &elem, nil
	}
	return nil, nil
}