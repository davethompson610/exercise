/*
Database API Unit Tests
*/

package api

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDbApi(t *testing.T) {
	// Cleanup the DB when done
	defer CleanupDb()

	// Initialize the databases to be used by the test
	DbListArg = "malicious;" + DbUriDefault + ";TestDb;Malicious,"
	DbListArg += "unsafe;" + DbUriDefault + ";TestDb;Unsafe"
	InitDbList()
	CleanupDb()
	InitDbCollections()
	assert.Equal(t, 2, len(DbList), "Wrong number of databases")

	// Verify the collection index was added to each database
	for dbId := range DbList {
		client, err := GetDbClient(dbId)
		assert.Nil(t, err, "Unable to create client")
		collection := GetDbCollectionUnsafeUrl(client, dbId)
		ok := DbIndexExists(collection, DbIndex{
			Key: map[string]int{
				DbCollectionDocPropAuthName:      1,
				DbCollectionDocPropPathQueryName: 1,
			},
			Name: DbCollectionIndexName,
		})
		assert.True(t, ok, "Collection index not found")
	}

	// Insert a URL which is unsafe
	url := UnsafeUrl{"google.com", "", "Trojan"}
	err := InsertDbOneUnsafeUrl(0, url)
	assert.Nil(t, err, "Error inserting into DB")

	// Verify we can find the URL
	var unsafeUrl *UnsafeUrl
	findUrl := FindUrl{url.Authority, url.PathQuery}
	unsafeUrl, err = FindDbAllUnsafeUrl(findUrl)
	assert.Nil(t, err, "Error querying DB")
	assert.NotNil(t, unsafeUrl, "Expected to find db doc")
	if unsafeUrl != nil {
		assert.Equal(t, url.Authority, unsafeUrl.Authority, "Incorrect authority")
		assert.Equal(t, url.PathQuery, unsafeUrl.PathQuery, "Incorrect path and query")
	}

	// Verify we do not find this url
	url = UnsafeUrl{"www.malwarebytes.com", "malware", "Spyware"}
	findUrl = FindUrl{url.Authority, url.PathQuery}
	unsafeUrl, err = FindDbAllUnsafeUrl(findUrl)
	assert.Nil(t, err, "Error querying DB")
	assert.Nil(t, unsafeUrl, "Expected not to find db doc")

	// Make the URL unsafe
	err = InsertDbOneUnsafeUrl(1, url)
	assert.Nil(t, err, "Error inserting into DB")

	// Verify we can't re-add the URL
	err = InsertDbOneUnsafeUrl(1, url)
	assert.NotNil(t, err, "Expecting error inserting into DB")
	assert.True(t, DbErrorDupKey(err), "Expected to find duplicate key error")

	// Verify we now can find this url
	unsafeUrl, err = FindDbAllUnsafeUrl(findUrl)
	assert.Nil(t, err, "Error querying DB")
	if unsafeUrl != nil {
		assert.Equal(t, url.Authority, unsafeUrl.Authority, "Incorrect authority")
		assert.Equal(t, url.PathQuery, unsafeUrl.PathQuery, "Incorrect path and query")
	}
}