package api

// Add command line args for the API package
func AddCmdArgs() {
	AddWebServiceCmdArgs()
	AddDbCmdArgs()
	AddUrlCacheCmdArgs()
}

// Run the API as a service
func RunApiAsService() {
	InitDbList()
	CheckDbConnections()
	InitDbCollections()
	RunWebService()
}