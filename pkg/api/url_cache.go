/*
URL Caching mechanism.
*/

package api

import (
	"flag"
	"log"
	"sync"
	"unsafe"
)

// Double linked list node
type DlNode struct {
	next, prev *DlNode
	onList     bool
	data       *UrlCacheData
}

// Get the next data instance linked to this node
func (node *DlNode) Next() *UrlCacheData {
	if node.next == nil {
		return nil
	}
	return node.next.data
}

// Get the previous data instance linked to this node
func (node *DlNode) Prev() *UrlCacheData {
	if node.prev == nil {
		return nil
	}
	return node.prev.data
}

// Double linked list
type DlList struct {
	head, tail *DlNode
	nodeCount  uint32
}

// Get the first data instance on this list
func (list *DlList) First() *UrlCacheData {
	if list.head == nil {
		return nil
	}
	return list.head.data
}

// Get the last data instance on this list
func (list *DlList) Last() *UrlCacheData {
	if list.tail == nil {
		return nil
	}
	return list.tail.data
}

// Add a node to the end of the list
func (list *DlList) PushBack(node *DlNode) {
	if node.onList {
		log.Fatal("Attempt to add node already on list")
	}
	if list.tail == nil {
		list.head = node
	} else {
		list.tail.next = node
	}
	node.prev = list.tail
	node.next = nil
	node.onList = true
	list.tail = node
	list.nodeCount++
}

// Move a node to the end of the list
func (list *DlList) MoveBack(node *DlNode) {
	if !node.onList || list.tail == node {
		return
	}
	if node.prev != nil {
		node.prev.next = node.next
	} else {
		list.head = node.next
	}
	node.next.prev = node.prev
	list.tail.next = node
	node.prev = list.tail
	node.next = nil
	list.tail = node
}

// Remove a node from the list
func (list *DlList) Remove(node *DlNode) {
	if !node.onList {
		return
	}
	if node.prev != nil {
		node.prev.next = node.next
	} else {
		list.head = node.next
	}
	if node.next != nil {
		node.next.prev = node.prev
	} else {
		list.tail = node.prev
	}
	node.prev = nil
	node.next = nil
	node.onList = false
	list.nodeCount--
}

// This is the data kept in the cache for each URL
type UrlCacheData struct {
	url            string
	urlStatus      UrlStatus
	malewareType   string
	usageListNode  DlNode
}

// Create a cache data instance
func CreateUrlCacheData(url string, urlStatus UrlStatus, malewareType string) *UrlCacheData {
	data := &UrlCacheData{url: url, urlStatus: urlStatus, malewareType: malewareType}
	data.usageListNode.data = data
	return data
}

// Remove a cache data instance from the cache (lock on cache must already be taken)
func (data *UrlCacheData) Remove(c *UrlCache) {
	if data.usageListNode.onList {
		delete(c.lookupMap, data.url)
		c.usageList.Remove(&data.usageListNode)
		c.usageSize -= uint64(len(data.url)) + uint64(unsafe.Sizeof(data))
	}
	data.usageListNode.data = nil
}

// This is the URL cache
type UrlCache struct {
	lookupMap      map[string]*UrlCacheData
	ageList        DlList
    usageList      DlList
	usageSize      uint64
	lock           sync.RWMutex
}

var UrlLookupCache = UrlCache{lookupMap: make(map[string]*UrlCacheData)}
var CacheUsageSizeMax uint64 = 1024 * 1024 * 1024

// Add command line args for the DB service
func AddUrlCacheCmdArgs() {
	flag.Uint64Var(&CacheUsageSizeMax, "cache-size-max", CacheUsageSizeMax, "Max size of cache")
}

// Take a write lock on the cache
func (cache *UrlCache) Lock() {
	cache.lock.Lock()
}

// Release a write lock on the cache
func (cache *UrlCache) Unlock() {
	cache.lock.Unlock()
}

// Take a read lock on the cache
func (cache *UrlCache) RLock() {
	cache.lock.RLock()
}

// Release a read lock on the cache
func (cache *UrlCache) RUnlock() {
	cache.lock.RUnlock()
}

// Clear the cache
func (cache *UrlCache) Clear() {
	cache.Lock()
	defer cache.Unlock()
	for cache.usageList.head != nil {
		cache.usageList.First().Remove(cache)
	}
}

// Add a URL to the cache
func (cache *UrlCache) AddUrl(url string, urlStatus UrlStatus, malewareType string) {
	// Check if cache is disabled
	if CacheUsageSizeMax == 0 {
		return
	}

	// Take a write lock on the cache
	cache.Lock()
	defer cache.Unlock()

	// If the entry already exists, update it
	if data, ok := cache.lookupMap[url]; ok {
		data.urlStatus = urlStatus
		data.malewareType = malewareType
	} else {
		// Add a new entry
		data = CreateUrlCacheData(url, urlStatus, malewareType)
		cache.lookupMap[data.url] = data
		cache.usageList.PushBack(&data.usageListNode)
		cache.usageSize += uint64(len(url)) + uint64(unsafe.Sizeof(data))

		// If we exceeded the cache size limit, remove least used entries until back in compliance
		for cache.usageSize > CacheUsageSizeMax {
			cache.usageList.First().Remove(cache)
		}
	}
}

// Lookup the URL in the cache. If not found in cache, returns UrlStatusUnknown
func (cache *UrlCache) Lookup(url string) (UrlStatus, string) {
	cache.RLock()
	data, ok := cache.lookupMap[url]
	if ok {
		// This entry is now our freshest. Move to end if not already there.
		if data.usageListNode.next != nil {
			cache.RUnlock()
			cache.Lock()
			cache.usageList.MoveBack(&data.usageListNode)
			cache.Unlock()
		} else {
			cache.RUnlock()
		}
		return data.urlStatus, data.malewareType
	} else {
		cache.RUnlock()
	}
	return UrlStatusUnknown, ""
}
