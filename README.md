# URL Lookup Service

We have an HTTP proxy that is scanning traffic looking for malware URLs. Before allowing HTTP connections to be made, this proxy asks a service that maintains several databases of malware URLs if the resource being requested is known to contain malware.

This project contains a small web service written in GO that responds to GET requests where the caller passes in a URL and the service responds with some information about that URL. The GET requests look like this:

*GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}*

The caller wants to know if it is safe to access that URL or not.

## Build and Run

This project requires the following GO packages:

* Gorilla Mux: `go get github.com/gorilla/mux`
* Mongo Driver: `go get go.mongodb.org/mongo-driver/mongo`
* Testify for UT: `go get github.com/stretchr/testify/assert`

The web service needs a mongo service running that it can talk to. An easy way to run mongo on your laptop is to run it in a docker instance like this: 

* `sudo docker run -d -p 27017:27017 -v ~/data:/data/db mongo`

UT can be ran like this:

* `cd pkg/api`
* `go test`

Benchmarks can be ran like this:

* `cd pkg/api`
* `go test -run XXX -bench . -benchtime=10s`

The web service can be built and run like this:

* `cd cmd/ur-info-service`
* `go build`
* `./url-info-service`

The following usage is supported on the url-info-service executable:

* `-cache-size-max uint`: Max size of cache (default 1073741824)
* `-databases string`: List of databases to be used (default "default;mongodb://localhost:27017;MalwareDb;UnsafeUrls").

The databases argument is a comma separated list of databases with properties for each database separated by semi-colons. The first property is the database name. When adding an unsafe URL, this name must be specified as the 'database'. The second property is the URI of the mongo instance to be used. The third property is the mongo database name and the fourth is the mongo collection name.

## Web Service Rest API

New unsafe URLs can be posted to the service like this (note if "database" not specified in the request body, then "default" will be used):

```
Request:
  POST urlinfo/1/url
  body = {
    "url":"google.eu/payme",
    "malwaretype":"Trojan"}
Response:
  body = {
    "url": "google.eu/payme",
    "malwaretype": "Trojan",
    "database": "default"
  }
```

Status of URLs can be checked like this:

```
Request:
  GET urlinfo/1/google.eu/payme
Response:
  body = {
    "url":"google.eu/payme",
    "status":"NotSafe",
    "malwaretype":"Trojan"
  }
```

## Design Considerations

I chose mongo (see db_api.go) as the database to persist the unsafe URL information pushed to the web service. This allows for the amount of URLs to grow beyond the memory limitations of the VM. Since queries to mongo are slow, I implemented a cache (see url_cache.go) to store the most recently checked URLs. The size of this cache is controlled by the CacheUsageSizeMax variable which can be set to a size best suiting the VMs memory size using the `-cache-size-max` argument. Whenever a URL is looked up in the cache, that URL is placed at the end of the usage cache list. When trimming of the cache is required, we trim from the front of the usage cache list as this will remove the least used cached entries first.

Whenever unsafe URLs are pushed to the service, the URL information is saved in mongo and added to the cache. If the cache entry already exists in the cache but with a different status (e.g. Safe), the status will be changed to reflec the new Unsafe status.

I added test benchmarks which compares GET operations without a cache (CacheUsageSizeMax is zero) with GET operations with a cache. Here are the results when running on my laptop:

```
davetho-mac:api$ go test -run XXX -bench . -benchtime=10s
goos: darwin
goarch: amd64
pkg: exercise/pkg/api
BenchmarkServiceApiWithoutCache-4   	     744	  15974021 ns/op
BenchmarkServiceApiWithCache-4      	 1337702	      8899 ns/op
PASS
ok  	exercise/pkg/api	36.113s
```

As can be seen there is quite a big penalty going to mongo so the more hits in cache the better.
