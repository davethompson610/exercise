/*
Web Service to provide information about a URL.
 */

package main

import (
	"exercise/pkg/api"
	"flag"
)

func main() {
	// Add and parse command args
	api.AddCmdArgs()
	flag.Parse()

	// This will start the web service
	api.RunApiAsService()
}
